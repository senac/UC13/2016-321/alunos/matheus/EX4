package br.com.senac.ex4;

public class Vendedores {

    private int codigo;
    private String nome;
    private double comissao = 0.05;

    public Vendedores() {
    }

    public Vendedores(int codigo, String nome) {
        this.codigo = codigo;
        this.nome = nome;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getComissao() {
        return comissao;
    }

}
