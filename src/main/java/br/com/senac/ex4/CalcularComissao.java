package br.com.senac.ex4;

public class CalcularComissao {

    public double Calcular(Pecas pecas, Vendedores vendedores) {
        return (pecas.getQuantidade() * pecas.getValorUnidade()) * vendedores.getComissao();
    }

}
