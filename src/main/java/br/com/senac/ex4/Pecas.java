package br.com.senac.ex4;

public class Pecas {

    private int codigo;
    private double ValorUnidade;
    private int quantidade;

    public Pecas(int codigo, double ValorUnidade, int quantidade) {
        this.codigo = codigo;
        this.ValorUnidade = ValorUnidade;
        this.quantidade = quantidade;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public double getValorUnidade() {
        return ValorUnidade;
    }

    public void setValorUnidade(double ValorUnidade) {
        this.ValorUnidade = ValorUnidade;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

}
