package br.com.senac.ex4;

import org.junit.Test;
import static org.junit.Assert.*;

public class CalcularComissaoTest {

    @Test
    public void deveCalcularComissaoDeCincoPorCentoSobreMilReais() {
        CalcularComissao cc = new CalcularComissao();
        Pecas p = new Pecas(1, 100, 10);
        Vendedores v = new Vendedores(1, "DSchade");
        double comissao = cc.Calcular(p, v);
        assertEquals(1000 * 0.05, comissao, 0.01);

    }

}
